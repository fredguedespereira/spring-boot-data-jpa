package br.edu.ifpb.pweb2.banco.business.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.edu.ifpb.pweb2.banco.business.model.Correntista;
import br.edu.ifpb.pweb2.banco.business.repository.CorrentistaDAO;
import br.edu.ifpb.pweb2.banco.util.PasswordUtil;

@Service
public class CorrentistaService {
	
	@Autowired
	private CorrentistaDAO correntistaDAO;
	
	@Transactional
	public void insertBD() {
		correntistaDAO.deleteAll();
		
		Correntista darwin = new Correntista();
		darwin.setEmail("darwin@ed.ac.uk");
		darwin.setNome("Charles Robert Darwin");
		darwin.setSenha(PasswordUtil.hashPassword("evolucao"));
		correntistaDAO.save(darwin);
		
		Correntista vincent = new Correntista();
		vincent.setEmail("vincent@vangoghmuseum.nl");
		vincent.setNome("Vincent Willem van Gogh");
		vincent.setSenha(PasswordUtil.hashPassword("starrynight"));
		correntistaDAO.save(vincent);
		
		Correntista gleiser = new Correntista();
		gleiser.setEmail("gleiser@dartmouth.edu");
		gleiser.setNome("Marcelo Gleiser");
		gleiser.setSenha(PasswordUtil.hashPassword("universo"));
		correntistaDAO.save(gleiser);
		
		Correntista sagan = new Correntista();
		sagan.setEmail("sagan@nasa.gov");
		sagan.setNome("Carl Sagan");
		sagan.setSenha(PasswordUtil.hashPassword("cosmos"));
		correntistaDAO.save(sagan);
		
		Correntista curie = new Correntista();
		curie.setEmail("marie.curie@ac-paris.fr");
		curie.setNome("Marie Sklodowska Curie");
		curie.setSenha(PasswordUtil.hashPassword("twonobels"));
		correntistaDAO.save(curie);
		
		Correntista admin = new Correntista();
		admin.setEmail("admin@spring-banco.com.br");
		admin.setNome("Administrador do Sistema");
		admin.setSenha(PasswordUtil.hashPassword("root123"));
		admin.setAdmin(true);
		correntistaDAO.save(admin);
		
	}

	public List<Correntista>findAll() {
		return correntistaDAO.findAll();
	}

	public Correntista findById(Integer correntistaId) {
		Optional<Correntista> c =  correntistaDAO.findById(correntistaId);
		return c.isPresent() ? c.get() : null;
	}
	

}
