package br.edu.ifpb.pweb2.banco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class SpringBootBancoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBancoApplication.class, args);
	}

}
