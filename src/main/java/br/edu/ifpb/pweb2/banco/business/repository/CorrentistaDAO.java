package br.edu.ifpb.pweb2.banco.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.ifpb.pweb2.banco.business.model.Correntista;

public interface CorrentistaDAO extends JpaRepository<Correntista, Integer> {
	
	public Correntista findByEmail(String email);
}
